Variables
---------

The loginOAuth.jsp file reads the variables from: /var/jira-home/cern.properties

* oauth_web = https://oauth.web.cern.ch
* oauth_client_secret = ############################################
* oauth_client_id = dev.its.cern.ch
* oauth_redirect_url = https://dev.its.cern.ch



URLs
----

* The server redirects the user to:

https://oauth.web.cern.ch/OAuth/Authorize?client_id=dev.its.cern.ch\&redirect_uri=https://dev.its.cern.ch\&response_type=code

* This returns a ${code} to the server then the server does:

https://oauth.web.cern.ch/OAuth/Token -X POST -d "code=${code}&grant_type=authorization_code&client_secret=${secret}&redirect_uri=https%3A%2F%2Fdev.its.cern.ch&client_id=dev.its.cern.ch"

* This returns the ${access_token} value that uised like this:

curl -H "uthorization: Bearer ${access_token}" https://oauth.web.cern.ch/OAuth/Token

Returns the JSON with user's data.

More information at: https://espace.cern.ch/authentication/CERN%20Authentication/OAuth.aspx
