<%@ page import="com.opensymphony.util.TextUtils" %> 
<%@ page import="org.apache.oltu.oauth2.client.URLConnectionClient" %>
<%@ page import="org.apache.oltu.oauth2.client.request.OAuthClientRequest" %>
<%@ page import="org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest" %>
<%@ page import="org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse" %>
<%@ page import="org.apache.oltu.oauth2.client.response.OAuthResourceResponse" %>
<%@ page import="org.apache.oltu.oauth2.client.OAuthClient" %>
<%@ page import="org.apache.oltu.oauth2.common.message.types.GrantType" %>
<%@ page import="org.apache.oltu.oauth2.common.exception.OAuthProblemException" %>
<%@ page import="org.apache.oltu.oauth2.common.exception.OAuthSystemException" %>
<%@ page import="org.apache.oltu.oauth2.common.OAuth.HttpMethod" %>
<%@ page import="java.net.URL" %>
<%@ page import="org.json.JSONTokener" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="java.util.Properties" %>
<%@ page import="java.io.FileInputStream" %>
<%
   final String code = request.getParameter("code");
   String redirectionTo = null;

   %><p><a href='https://dev.its.cern.ch/jira/loginOAuth.jsp'>Login Oauth</a></p><%

   if(code == null) {

	final String client_id = "dev.its.cern.ch";
	final String redirect_uri = "https://dev.its.cern.ch";

	redirectionTo = "https://oauth.web.cern.ch/OAuth/Authorize?client_id="
			+client_id+"&redirect_uri="+redirect_uri
			+"&response_type=code";
	response.sendRedirect(redirectionTo);
   } else {
/*
 	%> Code: <%= code %><br/><br/><%
 */
	
	String defaultPathToCernProperties = "/var/jira-home/cern.properties";
	Properties cernProperties = new Properties();
	FileInputStream fis = null;
	fis = new FileInputStream(defaultPathToCernProperties);
	cernProperties.load(fis);
	
	URL url = new URL(cernProperties.getProperty("oauth_web"));

	String CLIENT_SECRET = null;
//	if(cernProperties.getProperty("oauth_client_secret") != null)
	CLIENT_SECRET = cernProperties.getProperty("oauth_client_secret");
	String CLIENT_ID = cernProperties.getProperty("oauth_client_id");
	String REDIRECT_URL = cernProperties.getProperty("oauth_redirect_url");

	OAuthClientRequest oauth_request = OAuthClientRequest
                .tokenLocation(url.toString() + "/OAuth/Token")
                .setClientId(CLIENT_ID)
                .setClientSecret(CLIENT_SECRET)
                .setGrantType(GrantType.AUTHORIZATION_CODE)
                .setCode(code)
                .setRedirectURI(REDIRECT_URL)
                .buildBodyMessage();
//                .buildHeaderMessage();
//                .buildQueryMessage();

/*
		%>Request body: <%= oauth_request.getBody() %><br/>
                Request URI: <%= oauth_request.getLocationUri() %><br/>
                Curl: <pre>curl -i <%= url.toString() + "/OAuth/Token" %> -X POST -d "<%= oauth_request.getBody() %>"</pre>
		<br/>
		<br/><%
*/

        OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
	OAuthJSONAccessTokenResponse oAuthResponse = null;
	
	String accessToken = null;
	String refreshToken = null;
	String scope= null;
	try { 
        	oAuthResponse = oAuthClient.accessToken(oauth_request, HttpMethod.POST);
		accessToken = oAuthResponse.getAccessToken();
		refreshToken = oAuthResponse.getRefreshToken();
		scope = oAuthResponse.getScope();
	
/*
 		%>Access Token:  <%= accessToken %><br/>
		Expires in: <%= oAuthResponse.getExpiresIn()%><br/>
		Refresh Token: <%= refreshToken %><br/>
		Scope: <%= scope %><br/><%
*/
        } catch(OAuthProblemException oe) {
		%> ERROR: <%= oe.getError() %><br/>
		Description: <%= oe.getDescription() %><br/>
		Message: <%= oe.getMessage() %><br/>
		Status: <%= oe.getResponseStatus() %><br/>
		URI: <%= oe.getUri() %><br/>
		<%
	}

	URL userURL = new URL("https://oauthresource.web.cern.ch/api/User");

	 OAuthClientRequest bearerClientRequest = new OAuthBearerClientRequest(userURL.toString())
		.setAccessToken(accessToken)
		.buildHeaderMessage();
	
	OAuthResourceResponse resource_response = null;
	try {
		resource_response = oAuthClient.resource(bearerClientRequest, HttpMethod.GET, OAuthResourceResponse.class);
		JSONObject person = new JSONObject(resource_response.getBody());
		%>Name: <%= person.getString("name") %><br/>
		Username: <%= person.getString("username") %><br/>
		Email: <%= person.getString("email") %><br/>
		<%
	} catch (OAuthSystemException ose) {
		%>ERROR
		<%= ose.getMessage() %>
		<%
	}
   }

 
%>
